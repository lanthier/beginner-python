class Mage:
    # Built in intialization method, self can really be aliased as anything but it's good practice to use self
    def __init__(self, name, level):
        self.name = name
        self.level = level
        self.health = 5*level
        self.attack = level
        if(level > 1):
            self.spellAttack = Spell('Firebolt', 6)

    # Default method that gets called on print
    def __str__(self):
        return '--' + self.name + '--' + '\nClass: Mage\nLevel: ' + str(self.level) + '\nHealth: ' + str(self.health) + '\nAttack: ' + str(self.attack) + '\nSpell: ' + self.spellAttack.name

class Spell:
    def __init__(self, name, damage):
        self.name = name
        self.damage = damage

playerOne = Mage('Alex', 2)

print(playerOne)
