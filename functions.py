def first_function():
    print('Hello world from a function!')

def concat_strings(firstString, secondString):
    return firstString + secondString

def print_state(state = 'Michigan'): # Sets default to Michigan, similar to C#
    print('I am living in ' + state)

first_function()

concat_strings('race', 'car')

print_state()

print_state('Wisconsin')