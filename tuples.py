fam = ('Doug', 'Donna', 'Derek', 'Dee')
print(fam)
print(fam[3])

try:
    fam[3] = 'Not Dee'
except TypeError:
    print('Tuples do not support item assignments!')

aTuple = tuple(('Matt', 'Vanessa'))
print(len(aTuple))