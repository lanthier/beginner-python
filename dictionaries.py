favorites = {
    'animal': 'sunbear',
    'food': 'thai',
    'language': 'C#'
}

print(favorites)

favorites['language'] = 'python'
print(favorites)

typicalMeals = dict(breakFast='nil', lunch='salad', dinner='meat and cheese')
typicalMeals['snack'] = 'string cheese'

print(typicalMeals)
print(len(typicalMeals))