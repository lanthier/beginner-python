# Arithmetic

x = 3
y = 2
print(x+y)
print(x-y)
print(x*y)
print(x/y)
print(x**y) #expotentiation
print(x//y) #floor division

# Logic

print(x is y) #false
print(x is not y) #true

# Membership
aString = 'Alex'

print('A' in aString) #true

print('B' not in aString) #true