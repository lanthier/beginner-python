a = 0

if a: 
    print("0 is true!")
else: 
    print("0 is false!")

b = 1

if a > b:
    print("You are in a dream")
else:
    print("1 is greater than 0") #output

c = "1"

if b == c:
    print("The number '1' and the string '1' are the same thing.")
else:
    print("The number '1' and the string '1' are NOT the same thing.") #output
