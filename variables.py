## Basics

a = "Alex"
b = 4
c = "4"

try: 
    print (a + b)
except TypeError:
    print("Python will not let you add a number type to a string.")

print (a + c)

b = "Python will let you reassign numbers to strings"
print (b)

# Complex Numbers
d = 3 + 1j
print(d)

print(type(d))

# Casting

a = 3

# int to string
b = str(a)
print(type(b))

# int to float
b = float(a)
print(type(b))

# Float to int
b += .2
a = int(b)
print(type(a))
print(a) # Precision loss as expected

# Strings
a = 'A'
b = "A"

if a == b: 
    print("Wow it is the same thing!")

# You can add strings together
a = a + b
print(a) #AA

# You can multiply strings as well
b = b * 3
print(b)

#Length
print(len(b))

#Lowercase
print(b.lower())

#Getting input from command line and returning it in uppercase
x = input()
print(x.upper())