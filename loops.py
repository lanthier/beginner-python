# While loops
i = 1

while i <= 10:
    print(i)
    i+=1

# For loops

roommates = ['Justin', 'Mike', 'Robb', 'Grant']

for roommate in roommates:
    print(roommate)

for x in range(3):
    print(x)