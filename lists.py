#Lists
travelList = ['Peru', 'Phillipines', 'Thailand']
print(travelList)

travelList[1] = 'New Zealand'
print(travelList)

ketoList = list(('cheese', 'meat', 'vegetables'))
print(ketoList)

ketoList.append('almonds')
ketoList.remove('cheese')
print(ketoList)
print(len(ketoList))

ketoList2 = list(('cheese', 'ranch', 'burgers'))

ketoList.append(ketoList2)
print(ketoList)