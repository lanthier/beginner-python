# Sets
# Does not keep order, requires items to be hashable, no duplicates
# Use for speedy member checking if you do not care about order nor duplicates
aSet = { 'sunbear' }
aSet.add('red panda')

print(aSet)

aSet.add('sunbear') # Cannot add duplicates, this will do nothing
aSet.remove('red panda')

print(aSet)